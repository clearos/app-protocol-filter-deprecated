# ClearOS Protocol Filter
The ClearOS Protocol Filter powered by l7-filter has been deprecated.  Fear not... a new [Protocol Filter](https://www.egloo.ca/products/netify/features/protocol-filter) solution powered by Netify and nDPI is now available in ClearOS 7!  You can find more information here: https://www.egloo.ca/products/netify/features/protocol-filter

Some of the protocols that can be blocked include:
- Bittorrent
- Steam
- VNC
- Dropbox
- Skype
- OpenVPN
- PPTP
- and 160+ more

Note: some of the old protocols detected in l7-filter are now handled in a different way and implemented using the [Netify Application Filter](https://www.egloo.ca/products/netify/features/application-filter).  The following apps and web sites can be blocked:
- Facebook
- Instagram
- Snapchat
- Netflix
- YouTube
- and thousands more
